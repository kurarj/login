import React, {Component} from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton';
import '../App.css';

class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            passError: '',
            userError: ''
        }
    }

    onBlur(event, stateProp) {
        if (event.target.value.trim().length < 1) {
            this.setState({[stateProp]:'Campo obrigatório.'})
        }
    }

    onChange(event, stateProp) {
        if (event.target.value.trim().length > 0) {
            this.setState({[stateProp]: ''})
        }
    }

    render() {
        return (
            <form className="form">
                    <TextField 
                        hintText="Usuário" 
                        errorText={this.state.userError} 
                        onBlur={event => this.onBlur(event, 'userError')}
                        onChange={event => this.onChange(event, 'userError')} />
                    <TextField 
                        hintText="Senha" 
                        type="password" 
                        errorText={this.state.passError} 
                        onBlur={event => this.onBlur(event, 'passError')} 
                        onChange={event => this.onChange(event, 'passError')}/>
                    <RaisedButton className="button" label="Entrar" primary={true} />
            </form>
        )
    }
}

export default Login