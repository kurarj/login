import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/login'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Login/>
      </MuiThemeProvider>
    );
  }
}

export default App;
